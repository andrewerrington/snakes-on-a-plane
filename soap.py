#!/usr/bin/python3

#
# Copyright 2019 Andrew Errington
#
# This file is part of Snakes on a Plane (SOAP).
#
# SOAP is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SOAP is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SOAP.  If not, see <https://www.gnu.org/licenses/>.
#


import time

from HT16K33 import HT16K33
controller=[]
controller.append(HT16K33())    # controller[0]

keys=0
old_keys=0

def pump_callback():
    if l_pump.state is not None:
        print("L PUMP:", l_pump.state)
    if r_pump.state is not None:
        print("R PUMP:", r_pump.state)

def engine_callback():
    if l_engine.state is not None:
        print("L ENGINE:", l_engine.state)
    if r_engine.state is not None:
        print("R ENGINE:", r_engine.state)

def flaps_callback():
    if flaps.state is not None:
      print("FLAPS:", flaps.state)
    
def xflow_callback():
    if x_flow.state is not None:
      print("XFLOW:", x_flow.state)
      if x_flow.state == 'ON':
        x_flow_led.on()
      else:
        x_flow_led.off()

def xfer_callback():
    if x_fer.state is not None:
      print("XFER:", x_fer.state)
      if x_fer.state == 'ON':
        x_fer_led.on()
      else:
        x_fer_led.off()


class LED:
    """ A single LED on or off """
    
    # Don't forget to call writeDisplay periodically
    def __init__(self, controller, name, bit):
        self.controller = controller
        self.name = name
        self.bit = bit
        
    def on(self):
        self.controller.setBit(self.bit, update=False)

    def off(self):
        self.controller.clearBit(self.bit, update=False)


class switch:
    """ A single or multipole switch with an optional 'None' value."""

    def __init__(self, controller, name, bit_nums, states = ['OFF', 'ON'], callback = None):
        # bit_nums is a list of bit numbers for the connected switch inputs
        # states is a list of states for this switch corresponding with each bit
        # We assume that, due to the physical nature of the switch, only one input can be active.
        # Furthermore, if no bits are active then we return the state corresponding to
        # None in the bit_nums list

        # If bit_nums is a single integer then we create a list containing None and that integer
        # to make a simple on/off switch using the same structure as a multipole switch. It is
        # a multipole switch with one pole.
        self.controller = controller
        self.name = name

        if isinstance(bit_nums, list):
            # It's a list of bit numbers
            self.bit_nums = bit_nums # a list of bits set by this switch (default indicated by None)
        else:
            # Assume it's an int for a single bit number
            self.bit_nums = [None, bit_nums]
            
        self.states = states     # a list of states corresponding to each bit
        self.state = None
        self.last_state = None
        self.callback = callback

    def process_keys(self):
        # Assume that someone refreshes the keys periodically
        # getKeyBuffer() returns a RAM copy of the last key state
        bitmap = self.controller.getKeyBuffer()
        state = None
        
        for i, bit in enumerate(self.bit_nums):
            if bit is not None:
                if (bitmap & (1 << bit)):
                    state = i
        
        if state is not None:
            # We found the bit that was set
            self.state = self.states[state]
        elif None in self.bit_nums:
            # No bits were set, but we have a default for that
            self.state = self.states[self.bit_nums.index(None)]
        else:
            # No bits were set, and there's no default
            self.state = None
            
        #print(self.name, self.state)
                
        if self.state != self.last_state:
            # Do something
            print (self.name, "changed state from", self.last_state, "to", self.state)
            self.last_state = self.state
            # Something changed
            if self.callback is not None:
                self.callback()
            return True     # Something changed
        else:
            return False    # No change


# Switches

# Fuel
x_flow = switch(controller[0], "X-FLOW", 40, callback = xflow_callback)
x_fer = switch(controller[0], "X-FER", 41, callback = xfer_callback)

l_pump = switch(controller[0], "L PUMP", [None, 42, 43], ['OFF', 'AUTO', 'ON'], callback = pump_callback)
r_pump = switch(controller[0], "R PUMP", [None, 38, 39], ['OFF', 'AUTO', 'ON'], callback = pump_callback)

# Flaps
flaps = switch(controller[0], "FLAPS", [32, 33, 34, 35], ['0', '10', '20', '30'], callback = flaps_callback)

# Engine run
l_engine = switch(controller[0], "L ENGINE", 36, callback = engine_callback)
r_engine = switch(controller[0], "R ENGINE", 37, callback = engine_callback)

# Lighting
landing_l = switch(controller[0], "LANDING L", 18)
landing_r = switch(controller[0], "LANDING R", 19)

landing_n_ldg_taxi = switch(controller[0], "LANDING N LDG/TAXI", [20, None, 21], ['OFF', 'TAXI', 'N LDG/TAXI'])

emer_lts = switch(controller[0], "EMER LTS", [22, None, 23], ['OFF', 'ARM', 'ON'])

lighting_wing_insp = switch(controller[0], "LIGHTING WING INSP", 24)
lighting_nav = switch(controller[0], "LIGHTING NAV", 25)
lighting_strobe_bcn = switch(controller[0], "LIGHTING STROBE BCN", [26, None, 27], ['OFF', 'BCN', 'STROBE'])
lighting_smkg_belts = switch(controller[0], "LIGHTING SMKG/BELTS", [28, None, 29], ['OFF', 'BELTS', 'SMKG/BELTS'])


# LEDs
x_flow_led = LED(controller[0], "X-FLOW", 120)
x_fer_led = LED(controller[0], "X-FER", 119)



while(True):

  keys = (controller[0].refreshKeys())

  #if (keys != old_keys):
  #  print(format(keys,'#014x'))
  if (True):

    flaps.process_keys()

    l_engine.process_keys()
    r_engine.process_keys()

    x_flow.process_keys()

    x_fer.process_keys()

    l_pump.process_keys()      
    r_pump.process_keys()

    old_keys = keys
    controller[0].writeDisplay()

  time.sleep(0.1)
